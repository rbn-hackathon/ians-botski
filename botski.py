#!/bin/env python2

import time
import datetime
import os
from slackclient import SlackClient

# found at https://api.slack.com/web#authentication
token = os.environ['SLACK_TOKEN']
sc = SlackClient(token)
if sc.rtm_connect():
    poll = False
    poll_time = None
    poll_timeout = None
    taking_suggestions = False
    while True:
        channel_input = sc.rtm_read()
        if (len(channel_input) > 0) and ('text' in channel_input[0]):
            print channel_input[0]
            user_input = filter(None, channel_input[0]['text'].lower().split(' '))
            chan = channel_input[0]['channel']
            ts = channel_input[0]['ts']
            if ('@botski' and 'i' and 'am' and 'hungry') in user_input:
                sc.api_call(
                    "chat.postMessage", channel=chan, text="Would you like to vote on lunch?",
                        username='@botski', icon_emoji=':hamburger:'
                )
                sc.api_call("reactions.add", name="hamburger", channel=chan, timestamp=ts)
                poll = True
                poll_time = datetime.datetime.now()
                poll_timeout = datetime.datetime.now() + datetime.timedelta(minutes=5)


            if ('@botski' and 'yes') in user_input and poll:
                sc.api_call(
                    "chat.postMessage", channel=channel_input[0]['user'], text="Where to?",
                        username='@botski', icon_emoji=':hamburger:'
                )
                taking_suggestions = True

        if poll_time > poll_timeout:
            poll = False
            taking_suggestions = False

        time.sleep(1)
else:
    print "Connection Failed, invalid token?"
